using api.Context;
using api.Service.External;
using api.Service.User;
using Microsoft.EntityFrameworkCore;
using MySqlConnector;
using Prometheus;
var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

var connectionString = builder.Configuration.GetConnectionString("DevDatabase");
builder.Services.AddDbContext<DatabaseContext>(options =>
	options.UseMySql(
		connectionString,
		ServerVersion.AutoDetect(connectionString)
	)
);

builder.Services.AddSingleton<MinioService>();
builder.Services.AddScoped<UserService>();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
	using var scope = app.Services.CreateScope();
	var services = scope.ServiceProvider;
	var dbContext = services.GetRequiredService<DatabaseContext>();
	dbContext.Database.EnsureDeleted();
	dbContext.Database.EnsureCreated();
	var minioService = services.GetRequiredService<MinioService>();
	await minioService.EnsureBuckets();
	app.UseSwagger();
	app.UseSwaggerUI();
}

// var counter = Metrics.CreateCounter("api_path_counter", "", new CounterConfiguration()
// {
// 	LabelNames = new[] { "method", "endpoint" }
// });

// app.Use((context, next) =>
// {
// 	counter.WithLabels(context.Request.Method, context.Request.Path).Inc();
// 	return next();
// });

// app.UseMetricServer();
// app.UseHttpMetrics();
app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
