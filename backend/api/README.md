# api

This is a REST-API backend written in C#.

## Requirements

- [`dotnet ^6.0`](https://dotnet.microsoft.com/en-us/download/dotnet/thank-you/sdk-6.0.101-windows-x64-installer)
- [`docker/docker-compose`](https://docs.docker.com/engine/install/ubuntu/)

## Usage
- bootstrap the dev-environment
	- `docker-compose up -d`
- run the api
  - `dotnet run`


## Monitoring
The docker setup includes a Grafana/Prometheus instance. Currently only the minio is integrated.