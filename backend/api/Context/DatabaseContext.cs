using api.Model.Abstract;
using api.Model.User;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace api.Context;

public class DatabaseContext : DbContext
{
	public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
	{
		AttachEventListeners();
	}

	public DbSet<UserModel> Users { get; set; } = null!;


	protected override void OnModelCreating(ModelBuilder modelBuilder)
	{
		modelBuilder.Entity<UserModel>().ToTable("users");
	}

	private void AttachEventListeners()
	{
		ChangeTracker.StateChanged += UpdateTimestamps;
		ChangeTracker.Tracked += UpdateTimestamps;
	}

	private static void UpdateTimestamps(object? sender, EntityEntryEventArgs e)
	{
		if (e.Entry.Entity is not IWithTimestamps entityWithTimestamps) return;
		switch (e.Entry.State)
		{
			case EntityState.Deleted:
				entityWithTimestamps.DeletedAt = DateTime.Now;
				break;
			case EntityState.Modified:
				entityWithTimestamps.ModifiedAt = DateTime.Now;
				break;
			case EntityState.Added:
				entityWithTimestamps.CreatedAt = DateTime.Now;
				entityWithTimestamps.ModifiedAt = entityWithTimestamps.CreatedAt;
				break;
			case EntityState.Detached:
				break;
			case EntityState.Unchanged:
				break;
		}
	}

}