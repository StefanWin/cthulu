using Minio;
namespace api.Service.External;

public class MinioService
{
	const string UserProfileBucket = "user-profile-images";

	private static readonly MinioClient client = new MinioClient("localhost:9000", "admin", "adminadmin");

	public async Task EnsureBuckets()
	{
		if (!await client.BucketExistsAsync(UserProfileBucket))
		{
			await client.MakeBucketAsync(UserProfileBucket);
		}
	}

	public async Task UploadUserProfileImage(string userId, MemoryStream memoryStream, string contentType)
	{
		await client.PutObjectAsync(
			UserProfileBucket,
			userId,
			memoryStream,
			memoryStream.Length,
			contentType
		);
	}

	public async Task<Tuple<string?, byte[]?>> GetUserProfileImage(string userId)
	{
		var stat = await client.StatObjectAsync(UserProfileBucket, userId);
		if (stat != null)
		{
			var size = ((int)stat.Size);
			var data = new byte[size];
			using (var ms = new MemoryStream(((int)stat.Size)))
			{
				await client.GetObjectAsync(UserProfileBucket, userId, stream => stream.CopyTo(ms));
				data = ms.ToArray();
			}
			return new Tuple<string?, byte[]?>(stat.ContentType, data);
		}
		return new Tuple<string?, byte[]?>(null, null);
	}
}