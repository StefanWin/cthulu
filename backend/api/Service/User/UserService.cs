using api.Context;
using api.Model.User;
using api.Service.Abstract;
using api.Service.External;
using Microsoft.EntityFrameworkCore;

namespace api.Service.User;

public class UserService : Service<UserModel, UserDTO, UserInsert, UserUpdate>
{

  public UserService(DatabaseContext context) : base(context)
  {
  }


  public override bool Exists(string id)
  {
    return dbContext.Users.Any(u => u.Id == id);
  }

  public override async Task<IEnumerable<UserDTO>> GetAll()
  {
    return await dbContext.Users
      .Select(u => UserDTO.FromUser(u)!)
      .ToListAsync();
  }

  public override async Task<UserDTO?> GetById(string id)
  {
    var user = await dbContext.Users.FindAsync(id);
    return UserDTO.FromUser(user);
  }

  public override async Task<UserDTO> Insert(UserInsert insert)
  {
    var user = new UserModel()
    {
      Username = insert.Username,
      Password = insert.Password
    };
    dbContext.Users.Add(user);
    await dbContext.SaveChangesAsync();
    return UserDTO.FromUser(user)!;
  }

  public override async Task<UserDTO?> Update(string id, UserUpdate update)
  {
    if (id != update.Id)
    {
      throw new Exception("id's do not match");
    }
    var user = await dbContext.Users.FindAsync(id);
    if (user == null)
    {
      return null;
    }
    user.Username = update.Username;
    user.Password = update.Password;
    await dbContext.SaveChangesAsync();
    return UserDTO.FromUser(user);
  }

  public override async Task<string?> Delete(string id)
  {
    var user = await dbContext.Users.FindAsync(id);
    if (user == null)
    {
      return null;
    }
    dbContext.Users.Remove(user);
    await dbContext.SaveChangesAsync();
    return user.Id;
  }
}