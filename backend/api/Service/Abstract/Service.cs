using api.Context;
using api.Model.Abstract;

namespace api.Service.Abstract;

public abstract class Service<TModel, TDTO, TInsert, TUpdate>
  where TModel : BaseModel
{
  protected DatabaseContext dbContext { get; init; }

  protected Service(DatabaseContext context)
  {
    dbContext = context;
  }

  public abstract bool Exists(string id);
  public abstract Task<IEnumerable<TDTO>> GetAll();
  public abstract Task<TDTO?> GetById(string id);
  public abstract Task<TDTO> Insert(TInsert insert);
  public abstract Task<TDTO?> Update(string id, TUpdate update);
  public abstract Task<string?> Delete(string id);
}