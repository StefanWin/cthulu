namespace api.Model.Abstract;

public interface IWithOwner<T> where T : BaseModel
{
  public T? Owner { get; set; }
}