namespace api.Model.Abstract;

public abstract class BaseModel : IWithTimestamps
{
  public string Id { get; init; }
  public DateTime CreatedAt { get; set; }
  public DateTime ModifiedAt { get; set; }
  public DateTime? DeletedAt { get; set; }

  public BaseModel()
  {
    Id = Guid.NewGuid().ToString();
    CreatedAt = DateTime.Now;
    ModifiedAt = CreatedAt;
  }
}