namespace api.Model.Abstract;

public interface IWithTimestamps
{
  public DateTime CreatedAt { get; set; }
  public DateTime ModifiedAt { get; set; }
  public DateTime? DeletedAt { get; set; }
}