namespace api.Model.User;

public class UserUpdate
{
  public string Id { get; set; } = null!;
  public string Username { get; set; } = null!;
  public string Password { get; set; } = null!;
}