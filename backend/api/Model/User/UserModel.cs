using api.Model.Abstract;

namespace api.Model.User;

public class UserModel : BaseModel
{
  public string Username { get; set; } = null!;
  public string Password { get; set; } = null!;
}