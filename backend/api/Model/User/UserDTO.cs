using api.Model.Abstract;

namespace api.Model.User;

public class UserDTO : IWithTimestamps
{

  public string Id { get; set; } = null!;
  public DateTime CreatedAt { get; set; }
  public DateTime ModifiedAt { get; set; }
  public DateTime? DeletedAt { get; set; }

  public string Username { get; set; } = null!;

  public static UserDTO? FromUser(UserModel? user)
  {
    if (user == null)
    {
      return null;
    }
    return new UserDTO()
    {
      Id = user.Id,
      CreatedAt = user.CreatedAt,
      ModifiedAt = user.ModifiedAt,
      DeletedAt = user.DeletedAt,
      Username = user.Username
    };
  }
}