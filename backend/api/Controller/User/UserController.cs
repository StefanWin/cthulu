using System.IO;
using System.Net;
using System.Reflection;
using api.Model.User;
using api.Service.External;
using api.Service.User;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace api.Controller.User;

[Route("api/user")]
[ApiController]
public class UserController : ControllerBase
{

	private readonly UserService _userService;
	private readonly MinioService _minioService;

	public UserController(UserService userService, MinioService minioService)
	{
		_userService = userService;
		_minioService = minioService;
	}

	private async Task UploadProfileImage(IFormFile profileImage, string userId)
	{
		if (profileImage.Length > 0)
		{
			using (var ms = new MemoryStream(((int)profileImage.Length)))
			{
				await profileImage.CopyToAsync(ms);
				ms.Position = 0;
				await _minioService.UploadUserProfileImage(userId, ms, profileImage.ContentType);
			}
		}
	}

	[HttpGet]
	public async Task<IEnumerable<UserDTO>> GetAll()
	{
		return await _userService.GetAll();
	}

	[HttpGet("{id}")]
	public async Task<ActionResult<UserDTO?>> Get(string id)
	{
		var user = await _userService.GetById(id);
		return user is null ? NotFound() : user;
	}

	[HttpPost]
	public async Task<ActionResult<UserDTO>> Insert([FromForm] string user, [FromForm] IFormFile? profileImage)
	{
		UserInsert? userInsert = JsonConvert.DeserializeObject<UserInsert>(user);
		if (userInsert != null)
		{
			var newUser = await _userService.Insert(userInsert);
			if (profileImage != null)
			{
				if (!profileImage.ContentType.StartsWith("image/"))
				{
					return BadRequest();
				}
				await UploadProfileImage(profileImage, newUser.Id);
			}
			return newUser;
		}
		return BadRequest();
	}

	[HttpPut("{id}")]
	public async Task<ActionResult<UserDTO?>> Update(string id, UserUpdate update)
	{
		var user = await _userService.Update(id, update);
		return user is null ? NotFound() : user;
	}

	[HttpDelete("{id}")]
	public async Task<ActionResult<string?>> Delete(string id)
	{
		var deletedId = await _userService.Delete(id);
		return deletedId is null ? NotFound() : deletedId;
	}

	[HttpGet("profile-image/{userId}")]
	public async Task<ActionResult> GetUserProfileImage(string userId)
	{
		var tuple = await _minioService.GetUserProfileImage(userId);
		var contentType = tuple.Item1;
		var data = tuple.Item2;
		if (contentType == null || data == null)
		{

			return NotFound();
		}
		return File(data, contentType);
	}
}