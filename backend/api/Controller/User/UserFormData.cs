using api.Model.User;

namespace api.Controller.User;

public class UserInsertFormData
{
	public UserInsert User { get; set; } = null!;
	public IFormFile? ProfileImage { get; set; } = null!;
}

public class UploadedProfileImage
{
	public IFormFile ProfileImage { get; set; } = null!;
}

public class UserUpdateFormData
{
	public UserUpdate User { get; set; } = null!;
	public IFormFile ProfileImage { get; set; } = null!;
}