namespace api.Utility;

public static class APIMath
{
	public static int Add(int a, int b)
	{
		return a + b;
	}

	public static int Sub(int a, int b) => a - b;
}