namespace api.Utility;

public static class APIUrl
{
	public static string StripQueryParams(string url)
	{
		return url.Split("?")[0];
	}
}