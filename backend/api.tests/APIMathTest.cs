using api.Utility;
using Xunit;
namespace api.tests;

public class APIMathTest
{
	[Fact]
	public void Add_CorrectResult()
	{
		Assert.Equal(2, APIMath.Add(1, 1));
	}

	[Theory]
	[InlineData(1, 1, 2)]
	[InlineData(1, 0, 1)]
	[InlineData(0, 0, 0)]
	public void Add_CorrectResults(int a, int b, int result)
	{
		Assert.Equal(result, APIMath.Add(a, b));
	}

	[Fact]
	public void Sub_CorrectResult()
	{
		Assert.Equal(0, APIMath.Sub(1, 1));
	}
}