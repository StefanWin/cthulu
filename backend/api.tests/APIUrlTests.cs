using api.Utility;
using Xunit;
namespace api.tests;

public class APIUrlTest
{
	[Theory]
	[InlineData("http://foobar.com", "http://foobar.com")]
	[InlineData("http://www.foobar.com", "http://www.foobar.com")]
	public void StripQueryParams_NoQueryParams(string url, string expected)
	{
		Assert.Equal(expected, APIUrl.StripQueryParams(url));
	}

	[Theory]
	[InlineData("http://foobar.com?a=b", "http://foobar.com")]
	[InlineData("http://www.foobar.com?a=b&c=d", "http://www.foobar.com")]
	public void StripQueryParams_QueryParams(string url, string expected)
	{
		Assert.Equal(expected, APIUrl.StripQueryParams(url));
	}
}